**There are two parallel GPU finite rate finite temperature (FRFT) shear codes of Kob Anderson (KA) model with  DPD thermostat and Nose Hover thermostat, written in CUDA in this directory.**
 
 Note that center of mass velocity is not subtracted from each particle's velocities.
   
 * I have added a comment with capital letters "TWEAK THIS" ahead of those  	 variables which you can tweak and see the results
 	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, first install recommended nvidia driver and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile FRFT shear DPD code using: 
 *	 	 nvcc md_gpu_KobAndersonDPD_FRFTShear.cu -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w

 *	 Compile FRFT shear Nose Hover code using: 
 *	 	 nvcc -arch=sm_61 -rdc=true -lcudadevrt md_gpu_KobAndersonNoseHoverFRFTShear.cu -w -lm  -Xptxas -O3,-v   --use_fast_math -w

    
     -rdc=true  -lcudadevrt is used for dynamic parallelism
     Note that dynamic parallelism is only supported in V100 and later GPU
     cards. 
      
      -arch=sm_61 is the flag used for the GPU card architecture. 
      -arch=sm_61 is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on
     Tesla V100, change -arch=sm_61 to -arch=sm_70.



 *	 Run :
 *		 nohup ./a.out &
 

  Read input Data:<br/>

  Coordinate file   :: 		PositionLammpsPallabi.dat (N=4000, number density = 1.2, T = 0.466)<br/>
  Format:<br/><pre>
    Particle Id		type	x	y	z	mx	my	mz
    ....</pre>


  Velocity file   :: 		VelocityLammpsPallabi.dat<br/>
   Format:<br/><pre>
     Particle Id		vx	vy	vz
     ....</pre>

![KAFRFTGPU](/uploads/a3741d6465bb1475dd31647274229105/KAFRFTGPU.png)
  
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020
   

  

